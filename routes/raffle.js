(function(exports){
   var redis = require("redis"),
       client = redis.createClient(),
       og = require('open-graph'),
       Hash = require('shorthash').unique,
       secrets = require('../secrets.json');

       exports.new = function(req, res){
         res.render("new-raffle", {title:"Crie uma nova URL"});
       };

       exports.create = function(req,res){
         var url = req.body.url;

           if(url){
             og(url,function(er,meta){
               var newId = Hash(url);
               if(er){
                 meta = {
                   id: newId,
                   title: newId,
                   url: url,
                   type: "website"
                 };
               }else{
                 meta.id = newId;
                 meta.title = meta.title||newId;
               }

               client.HMSET('url', newId, JSON.stringify(meta),function(er){
                 res.render("new-url",{"title": meta.title, "url": meta});
               });
             });
           }
       };

       exports.load = function(req,res){
         var hash = req.params.hash;
         client.HMGET('url', hash,function(er,data){
           if(er || !data || data.length === 0 || (data.length == 1 && data[0] === null)){
            req.headers.url = req.originalUrl;
            res.render('404',req.headers);
           }else{
            data = JSON.parse(data);
            res.render("url",{"title": data.title, "url": data});
            data.count = (data.count||0)+1;
            client.HMSET("url",hash,JSON.stringify(data));
           }
         });
       };

       exports.list = function(req,res){
         client.HGETALL("url", function (err, list) {
              for(i in list){
                list[i] = JSON.parse(list[i]);
              }
              res.render('url-list',{"list":list});
          });
       };

})(exports);
